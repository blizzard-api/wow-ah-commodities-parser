package main

func ParseAHData(data []byte) {
	index := 0

	offset := 0
	for {
		if data[index] == ':' {
			offset += 1
		}
		if offset == 5 {
			index++
			break
		}
		index++
	}

	blockCounter := 0

	index++
	start := index
	for {
		if data[index] == '{' {
			blockCounter += 1
			if blockCounter == 1 {
				start = index
			}
		}
		if data[index] == '}' {
			blockCounter -= 1
			if blockCounter == 0 {
				WG.Add(1)
				Entries <- data[start : index+1]
				index++
				if data[index] == ']' {
					break
				}
			}
		}
		index++
	}
}

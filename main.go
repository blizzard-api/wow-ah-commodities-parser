package main

import (
	"encoding/json"
	"flag"
	"fmt"
	blizzard_api "github.com/francis-schiavo/blizzard-api-go"
	"github.com/joho/godotenv"
	"log"
	"net/http"
	"sync"
	"time"
)

var Entries chan []byte
var WG *sync.WaitGroup

type Commodity struct {
	Id   int `json:"id" pg:",pk"`
	Item struct {
		Id int `json:"id"`
	} `json:"item"`
	Quantity  int    `json:"quantity"`
	UnitPrice int    `json:"unit_price"`
	TimeLeft  string `json:"time_left"`
}

func parseEntry() {
	var commodity Commodity
	for data := range Entries {
		WG.Done()
		err := json.Unmarshal(data, &commodity)
		if err != nil {
			continue
		}

		result, err := dbConn.Model(&commodity).Insert()
		if err != nil || result.RowsAffected() != 1 {
			log.Fatal("Failed to insert record.")
		}
	}
}

func main() {
	dbConcurrency := flag.Int("database-pool", 1, "Defines how many connections to the DB should be used.")
	tm := flag.Int("api-timeout", 60, "Max wait time for API calls in seconds")
	flag.Parse()

	_ = godotenv.Load()

	config := &Config{}
	config.LoadFromEnvironment()

	Connect(config.Username, config.Password, config.Database, *dbConcurrency, config.Hostname, config.Port)
	createTable(&Commodity{})

	Entries = make(chan []byte, *dbConcurrency*2)
	for i := 0; i < *dbConcurrency; i++ {
		go parseEntry()
	}
	WG = &sync.WaitGroup{}

	//redisClient := redis.NewClient(&redis.Options{
	//	Addr: "127.0.0.1:6379",
	//	DB:   0,
	//})
	//cacheProvider := blizzard_api.NewRedisCache(redisClient)

	transport := &http.Transport{
		IdleConnTimeout:       60 * time.Second,
		ResponseHeaderTimeout: 60 * time.Second,
	}
	timeout := time.Duration(*tm) * time.Second
	apiClient := blizzard_api.NewWoWClient(blizzard_api.US, nil, false, 1, transport, &timeout)
	apiClient.CreateAccessToken(config.ClientID, config.ClientSecret, blizzard_api.US)
	fmt.Println("Fetching commodities data.")
	apiData := apiClient.AuctionCommodities(nil)

	fmt.Println("Parsing commodities data.")
	ParseAHData(apiData.Body)
	WG.Wait()
}

package main

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

type Config struct {
	ClientID     string `json:"client_id"`
	ClientSecret string `json:"client_secret"`

	Username string `json:"username"`
	Password string `json:"password"`
	Database string `json:"database"`
	Hostname string `json:"hostname"`
	Port     int    `json:"port"`
}

func (c *Config) LoadFromFile(filename string) {
	byteValue, err := ioutil.ReadFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	parseError := json.Unmarshal(byteValue, c)
	if parseError != nil {
		log.Fatal(err)
	}
}

func (c *Config) LoadFromEnvironment() {
	c.Hostname = os.Getenv("HOSTNAME")
	c.Port, _ = strconv.Atoi(os.Getenv("PORT"))
	c.Username = os.Getenv("USERNAME")
	c.Password = os.Getenv("PASSWORD")
	c.Database = os.Getenv("DATABASE")

	c.ClientID = os.Getenv("CLIENT_ID")
	c.ClientSecret = os.Getenv("CLIENT_SECRET")
}
package main

import (
	"fmt"
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
	"log"
)

var dbConn *pg.DB

// Connect is used to create the Postgres connection pool
func Connect(username string, password string, db string, poolSize int, hostname string, port int) {
	dbConn = pg.Connect(&pg.Options{
		Addr:     fmt.Sprintf("%s:%d", hostname, port),
		User:     username,
		Password: password,
		Database: db,
		PoolSize: poolSize,
	})
}

// GetDBConn is a safer way to access the connection pool
func GetDBConn() *pg.DB {
	if dbConn != nil {
		return dbConn
	}
	log.Println("database connection not active yet")
	return nil
}

func createTable(model interface{}) {
	err := GetDBConn().Model(model).CreateTable(&orm.CreateTableOptions{
		IfNotExists:   true,
		FKConstraints: true,
	})
	if err != nil {
		panic(err)
	}
}
